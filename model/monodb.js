const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/schools_management', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})  .then(() => console.log('Connection Successful....'))
    .catch(err =>console.log('failed to connect to mongodb',err));
 
//Connecting Node and MongoDB
require('./school.model');
require('./student.model');
require('./users.model')