const mongoose = require('mongoose');
const Joi = require('joi')
const jwt = require('jsonwebtoken');

var UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: "Name is required for the User"
    },
    email: {
        type: String,
        required: "Email is required for the User"
    },
    schoolId: {
        type: String,
        required: "SchoolId is necessary"
    },
    password: {
        type: String,
        required: "Password is necessary"
    },
    isAdmin: {
        type: Boolean,
        required: true,
        default: true
    }
})
UserSchema.methods.generateAuthenticationToken = function(){
    const token = jwt.sign({_id: this._id, name: this.name, email: this.email, isAdmin: this.isAdmin, schoolId: this.schoolId},'12345')
    return token;
}

const User = mongoose.model('User', UserSchema);

function validateUser(User){
    const schema = {
        name:Joi.string().max(255).min(3).required(),
        email: Joi.string().max(255).min(3).required().email(),
        schoolId: Joi.string().max(255).min(3).required(),
        password:  Joi.string().max(255).required(),
        isAdmin: Joi.boolean()
    }
    return Joi.validate(User, schema)
}
module.exports.User = User;
module.exports.validate = validateUser;

