//Import the dependencies
const hashPassword = require('../utils/hash')
const _= require('lodash')
const express = require('express');
const {User,validate} =  require('../model/users.model')
//Creating a Router
var router = express.Router();

//get users
router.get('/',async (req,res)=>{
    const users = await User.find().sort({name:1});

    return res.send(users)
});
router.get('/:email',async (req,res)=>{
    const users = await User.find({email: req.params.email});
    return res.send(users)
});
router.post('/',async (req,res) =>{
    const {error} = validate(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    let Users  = await User.findOne({email:req.body.email})
    if(Users) return res.send('User already registered').status(400)

     /* user  =  new User({
        name:req.body.name,
        email : req.body.email,
        password : req.body.password
    }); */
   Users = new User(_.pick(req.body, ['name', 'email', 'password', 'schoolId', 'isAdmin']));
   const hashedPassword = await hashPassword(Users.password)
   Users.password = hashedPassword;
    await Users.save();
    return res.send(_.pick(Users, ["_id", 'name', 'email', 'isAdmin', 'schoolId'])).status(201)
});

module.exports = router;