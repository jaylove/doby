require('./model/monodb')
const express = require('express');
const bodyparser = require('body-parser')
const studentController = require('./controllers/studentController')
const schoolController = require('./controllers/schoolController')
let app = express();
const teacherController = require('./controllers/UserCintroller');
const User = require('./auth/userauths')



app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use(express.json());
 
app.get('/', (req, res) => {
    res.send('Welcome to our app');
});


app.use('/api/students', studentController);
app.use('/api/schools',  schoolController);
app.use('/api/users', teacherController);
app.use('/api/auth',User);

const port = process.env.PORT || 4545;

app.listen(port, () => console.log(`Server running on port ${port} 🔥`));